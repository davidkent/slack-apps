import channels
import datetime
import os
import prod_audit_config
import slack_api
import sys
import slack_message_templates
import users

from prod_audit_file_analyzer import AuditFileAnalyzer

RUN_ARGS_YESTERDAY = "--yesterday"
RUN_ARGS_TODAY = "--today"
RUN_ARGS_DATE_PREFIX = "--date="

command_line_args = sys.argv
if len(sys.argv) != 2:
    raise(ValueError('Audit date required. Argument Options: --today, --yesterday, --date=YYYY-MM-DD'))

prod_audits_date_arg = sys.argv[1]
prod_audits_date = datetime.datetime.today()
if prod_audits_date_arg == RUN_ARGS_YESTERDAY:
    prod_audits_date = datetime.datetime.today() - datetime.timedelta(days=1)
elif prod_audits_date_arg.startswith(RUN_ARGS_DATE_PREFIX):
    prod_audits_date = datetime.datetime.strptime(prod_audits_date_arg[len(RUN_ARGS_DATE_PREFIX):], "%Y-%m-%d")
elif prod_audits_date_arg != RUN_ARGS_TODAY:
    raise(ValueError('Invalid argument: {}'.format(prod_audits_date_arg)))

audit_results = prod_audit_config.AUDITS

for base_audit_filename in prod_audit_config.AUDIT_FILES:
    prod_audits_file = prod_audit_config.PROD_AUDIT_FILENAME_FORMAT.format(base_audit_filename, prod_audits_date)
    if not os.path.exists(prod_audits_file) : continue
    analyzer = AuditFileAnalyzer(audit_results)
    analyzer.load_from_file(prod_audits_file)

prod_audits_passed = True
for key in audit_results:
    if audit_results[key].threshold_breached:
        prod_audits_passed = False

m_slack_api = slack_api.SlackApi(prod_audit_config.SLACK_OAUTH_TOKEN)

channels_json = m_slack_api.get_all_channels_json()
my_users = users.Users([], m_slack_api)
my_channels = channels.Channels(channels_json, my_users)
post_channel = my_channels.get_channel_for_name(prod_audit_config.SLACK_CHANNEL_NAME)

audit_results_message = slack_message_templates.create_audit_slack_post(
    prod_audits_passed, audit_results.values(), prod_audits_date)
m_slack_api.post_message(audit_results_message, post_channel.id)
