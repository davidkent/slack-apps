TAG_COLUMN_NAME = 'tag'
GROUP_COLUMN_NAME = 'group'
TRADER_COLUMN_NAME = 'trader'

class AuditFileDataExtractor:
    def __init__(self, csv_header):
        self._column_indices = {}
        columns = csv_header.split(',')
        for i in range(len(columns)):
            cleansed_column_name = columns[i].strip().lower()
            self._column_indices[cleansed_column_name] = i

    def get_audit_name(self, csv_line_data):
        split_line = csv_line_data.split(',')
        return split_line[self._column_indices[TAG_COLUMN_NAME]]

    def add_data_to_audit(self, csv_line_data, audit):
        split_line = csv_line_data.split(',')
        group = split_line[self._column_indices[GROUP_COLUMN_NAME]]
        trader = split_line[self._column_indices[TRADER_COLUMN_NAME]]
        audit_value = float(split_line[self._column_indices[audit.column]])
        audit.add_data_point(audit_value, group, trader, csv_line_data)
