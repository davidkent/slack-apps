import slack_api

JSON_KEY_ID = 'id'
JSON_KEY_TEAM_ID = 'team_id'
JSON_KEY_NAME = 'name'
JSON_KEY_REAL_NAME = 'real_name'
JSON_KEY_DELETED = 'deleted'
JSON_KEY_PROFILE = 'profile'

class User:
    def __init__(self, c_id, c_name, c_real_name, c_team_id):
        self.id = c_id
        self.name = c_name
        self.real_name = c_real_name
        self.team_id = c_team_id

    @classmethod
    def fromJson(cls, parsed_json):
        real_name = 'none'
        if JSON_KEY_REAL_NAME in parsed_json:
            real_name = parsed_json[JSON_KEY_REAL_NAME]
        elif JSON_KEY_PROFILE in parsed_json:
            profile = parsed_json[JSON_KEY_PROFILE]
            if JSON_KEY_REAL_NAME in profile:
                real_name = profile[JSON_KEY_REAL_NAME]
        return cls(parsed_json[JSON_KEY_ID],
                   parsed_json[JSON_KEY_NAME],
                   real_name,
                   parsed_json[JSON_KEY_TEAM_ID])

    def __str__(self):
        return self.name

UNKNOWN_USER = User('UNKNOWN_ID', 'unknown', 'International Man of Mystery', 'no_team')


class Users:
    def __init__(self, users_json_arr, m_slack_api):
        self._users = {}
        self._deleted_users = {}
        self._empty = True
        if len(users_json_arr) == 0:
            return

        self._slack_api = m_slack_api
        for user in users_json_arr:
            new_user = User.fromJson(user)
            if user[JSON_KEY_DELETED]:
                new_user.name = new_user.name + "[DELETED]"
            self._users[new_user.id] = new_user

    def get_user_for_id(self, user_id):
        if self._empty:
            return UNKNOWN_USER
        if user_id in self._users:
            return self._users[user_id]
        else:
            user_json = self._slack_api.try_get_unknown_user(user_id)
            new_user = User.fromJson(user_json)
            self._users[user_id] = new_user
            return new_user

