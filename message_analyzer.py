import slack_api_utils

COLUMN_HEADERS = ["Channel", "Post Time", "Post Creator", "Replies", "Reply Users",
                  "First Reply", "Most Replies", 'Slack Link']


def print_csv_header():
    print(','.join(COLUMN_HEADERS))


def print_line_for_message(channel, msg):
    channel_name = channel.name
    post_time = str(msg.post_time)
    post_creator = msg.user.name
    reply_count_str = str(msg.reply_count)
    reply_user_names = []
    for reply_user in msg.reply_users:
        reply_user_names.append(reply_user.name)
    reply_users = ';'.join(reply_user_names)
    replies = msg.get_replies()
    first_reply = ''
    reply_counts = {}
    for reply in replies:
        if len(first_reply) == 0:
            reply_user_name = reply.user.name
            if reply_user_name is not msg.user.name:
                first_reply = reply_user_name
        if reply.user.name not in reply_counts:
            reply_counts[reply.user.name] = 0
        reply_counts[reply.user.name] = reply_counts[reply.user.name] + 1
    most_replies = 0
    most_reply_user = 'none'
    for reply_user in reply_counts.keys():
        temp_reply_count = reply_counts[reply_user]
        if temp_reply_count > most_replies:
            most_replies = temp_reply_count
            most_reply_user = "{}[{}]".format(reply_user, temp_reply_count)

    slack_link = slack_api_utils.slack_link_for_message(channel, msg)
    values = ','.join([channel_name, post_time, post_creator, reply_count_str, reply_users,
                       first_reply, most_reply_user, slack_link])
    print(values)
