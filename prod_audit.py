# Starting with a simple class for a prod audit. May want to eventually convert into some sort of interface
# with multiple implementations for various classes of audit. For now, simple "exceeds max value"

class ProdAudit:
    def __init__(self, audit_name, group_filter, excluded_traders, audit_threshold, audit_units, value_column):
        self.name = audit_name
        self._group_filter = group_filter
        self._excluded_traders = excluded_traders
        self.threshold = audit_threshold
        self.units = audit_units
        self.column = value_column
        self._data_points = []
        self.threshold_breached = False
        self._breaching_data_point = None
        self._worst_data_point = None
        self._breaches = []

    def num_breaches(self):
        return len(self._breaches)

    def num_data_points(self):
        return len(self._data_points)

    def worst_breach(self):
        return self._worst_data_point[0]

    def add_data_point(self, data_point_value, group, trader, data_point_text):
        if len(self._group_filter) > 0 and group not in self._group_filter:
            return
        if len(self._excluded_traders) > 0 and trader in self._excluded_traders:
            return

        data_point = [data_point_value, data_point_text]
        self._data_points.append(data_point)
        if data_point_value < self.threshold:
            return
        self.threshold_breached = True
        self._breaches.append(data_point)
        if not self._breaching_data_point or data_point_value < self._breaching_data_point[0]:
            self._breaching_data_point = data_point
        if not self._worst_data_point or data_point_value > self._worst_data_point[0]:
            self._worst_data_point = data_point

    def __str__(self):
        audit_pass_fail = "FAIL" if self.threshold_breached else "PASS"
        worst_value = -1 if self._worst_data_point is None else self._worst_data_point[0]
        worst_details = "" if self._worst_data_point is None else self._worst_data_point[1]
        return "Audit[{}]: {} - Data Points[{}], Breaches[{}], Threshold[{} {}], Worst[{}], Details[{}]".format(
            self.name, audit_pass_fail, len(self._data_points), len(self._breaches),
            self.threshold, self.units, worst_value, worst_details)
