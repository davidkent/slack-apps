
from prod_audit import ProdAudit

SLACK_OAUTH_TOKEN = "xoxb-15587958630-3227039295106-44UaoS2tiDlrk7ZVPKKNz08N"
SLACK_CHANNEL_NAME = "production_audits"

AUDIT_FILES = ["NumericEvent", "BooleanEvent", "LatencyEvent"]

PROD_AUDIT_FILENAME_FORMAT = "O:\ProductionAudits\{}_{:%Y-%m-%d}.csv"

AUDITS = {
    "AddOptionsLeg": ProdAudit("AddOptionsLeg", ["T1"], ["JPATEL", "POBRIEN"], 50, "ms", "median"),
    "Shown": ProdAudit("Shown", ["T1"], ["JPATEL", "POBRIEN"], 500, "ms", "median"),
    "DeleteOldRecords_Succeeded": ProdAudit("Weekend Record Deletion", [], [], 1, "Failures", "false"),
    "AsIfFitDialog_Shown": ProdAudit("AsIfFit Dialog Opened", [], ["JPATEL", "POBRIEN"], 500, "ms", "median"),
    "MediumOrderEntry_VisibleChanged": ProdAudit("Medium Order Entry", [], ["JPATEL", "POBRIEN"], 100, "ms", "median"),
    "AddNewTimeSalesRowsUnsafe": ProdAudit("TimeSales Add", [], ["JPATEL", "POBRIEN", "JHARTUNG", "MMILLER", "PLARSON"], 50, "ms", "median"),
    "_refreshTimer_Tick": ProdAudit("Hotlist Refresh", [], ["JPATEL", "POBRIEN"], 250, "ms", "median"),
    "AddNewTradeInternal": ProdAudit("TradeLog Add", [], ["JPATEL", "POBRIEN", "JHARTUNG", "MMILLER", "PLARSON"], 150, "ms", "median"),
}

