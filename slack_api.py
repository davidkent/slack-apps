import json
import requests
import slack_api_utils
import time

USERS_URL = "https://slack.com/api/users.list?"
USER_INFO_URL = "https://slack.com/api/users.info?"
CONVERSATIONS_URL = "https://slack.com/api/conversations.list?"
CHANNEL_MESSAGES_URL = "https://slack.com/api/conversations.history?"
MESSAGE_THREAD_URL = "https://slack.com/api/conversations.replies?"
MESSAGE_THREAD_PARAMS = {'channel': '', 'ts': ''}

POST_MESSAGE_URL = "https://slack.com/api/chat.postMessage"
POST_MESSAGE_JSON_KEY_BLOCKS = 'blocks'
POST_MESSAGE_JSON_KEY_CHANNEL = 'channel'

JSON_KEY_CHANNELS_ARRAY = 'channels'
JSON_KEY_USER_ARRAY = 'members'
JSON_KEY_MESSAGES_ARRAY = 'messages'
JSON_KEY_USER = 'user'

CHANNEL_URL_PARAM = 'channel'
THREAD_TS_URL_PARAM = 'ts'
USER_URL_PARAM = 'user'

HTTP_STATUS_CODE_OK_200 = 200
HTTP_STATUS_CODE_RATE_LIMITED_429 = 429
HTTP_RESPONSE_HEADER_RETRY_AFTER = 'retry-after'

DEFAULT_MAX_PAGE_SIZE = 4096
CHANNEL_MESSAGES_PAGE_SIZE = 500


class SlackApi:
    def __init__(self, oauth_token):
        self._use_cache = False
        self._slack_cache = None
        self._headers = {'Authorization': 'Bearer %s' % oauth_token,
                         'Content-Type': 'application/json; charset=utf-8'}

    def set_cache(self, slack_cache):
        if not slack_cache.is_loaded():
            raise RuntimeError('Cannot use unloaded cache. Initialize it!')

        self._slack_cache = slack_cache
        self._use_cache = True

    def _cache_get_json(self, url):
        response_text = self._slack_cache.get_result_for_url(url)
        if len(response_text) == 0:
            raise ConnectionError('No result found for url: {}'.format(url))
        return json.loads(response_text)

    def _http_get_json(self, url):
        get_response = requests.get(url, headers=self._headers)
        get_status_code = get_response.status_code
        get_response_text = get_response.text
        if get_status_code == HTTP_STATUS_CODE_RATE_LIMITED_429:
            retry_after_seconds = int(get_response.headers[HTTP_RESPONSE_HEADER_RETRY_AFTER])
            print("Rate Limited[{}]: retry-after: {}s - sleeping...".format(url, retry_after_seconds))
            time.sleep(retry_after_seconds)
            print('Awake. Retry...')
            get_response = requests.get(url, headers=self._headers)
            get_status_code = get_response.status_code
            get_response_text = get_response.text

        if get_status_code != HTTP_STATUS_CODE_OK_200:
            slack_api_utils.handle_error_response(url, get_response_text, get_status_code)

        return slack_api_utils.handle_ok_response(url, get_response_text)

    def _http_post_json(self, url, post_json):
        print("POST[{}]: {}".format(url, post_json))
        post_response = requests.post(url, json=post_json, headers=self._headers)
        post_response_status_code = post_response.status_code
        post_response_text = post_response.text
        print("Response[{}]: {}".format(post_response_status_code, post_response_text))

    def get_all_users_json(self):
        return self._get_json_array_paged_data(USERS_URL, {}, JSON_KEY_USER_ARRAY)

    def get_all_channels_json(self):
        return self._get_json_array_paged_data(CONVERSATIONS_URL, {}, JSON_KEY_CHANNELS_ARRAY)

    def get_channel_messages_json(self, channel_id):
        channel_message_params = {CHANNEL_URL_PARAM: channel_id}
        return self._get_json_array_paged_data(
            CHANNEL_MESSAGES_URL, channel_message_params, JSON_KEY_MESSAGES_ARRAY, CHANNEL_MESSAGES_PAGE_SIZE)

    def get_thread_messages_json(self, channel_id, thread_ts):
        thread_params = {CHANNEL_URL_PARAM: channel_id, THREAD_TS_URL_PARAM: thread_ts}
        return self._get_json_array_paged_data(MESSAGE_THREAD_URL, thread_params, JSON_KEY_MESSAGES_ARRAY)

    def try_get_unknown_user(self, user_id):
        user_params = {USER_URL_PARAM: user_id}
        return self._get_json_data(USER_INFO_URL, user_params, JSON_KEY_USER)

    def post_message(self, message_blocks, channel_id):
        message_json = {POST_MESSAGE_JSON_KEY_CHANNEL: channel_id,
                        POST_MESSAGE_JSON_KEY_BLOCKS: message_blocks}
        self._http_post_json(POST_MESSAGE_URL, message_json)

    def _get_json_data(self, base_url, params, data_key):
        url = slack_api_utils.encode_url_with_params(base_url, params)
        result_json = self._cache_get_json(url) if self._use_cache else self._http_get_json(url)
        return result_json[data_key]

    def _get_json_array_paged_data(self, base_url, params, data_key, max_items=DEFAULT_MAX_PAGE_SIZE):
        data_json = []
        next_page_cursor = slack_api_utils.FIRST_PAGE_NEXT_CURSOR
        while next_page_cursor is not slack_api_utils.LAST_PAGE_NEXT_CURSOR and len(data_json) < max_items:
            url = slack_api_utils.encode_url_with_params_and_pagination(base_url, params, next_page_cursor)
            result_json = self._cache_get_json(url) if self._use_cache else self._http_get_json(url)
            data_json.extend(result_json[data_key])
            next_page_cursor = slack_api_utils.get_next_page_cursor(result_json)

        return data_json
