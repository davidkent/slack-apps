class SlackHttpCache:
    def __init__(self):
        self._results_cache = {}

    def load_from_file(self, filename):
        openfile = open(filename)
        for line in openfile.readlines():
            split_line = line.split(' ')
            self._add_line_to_cache(split_line[0], split_line[1:])

    def _add_line_to_cache(self, line_start, rest_of_line):
        if not line_start.startswith('GET['):
            return

        url = line_start[4:-2]
        reconstituted_line = ' '.join(rest_of_line)
        self._results_cache[url] = reconstituted_line

    def is_loaded(self):
        return len(self._results_cache) > 0

    def get_result_for_url(self, url):
        if url in self._results_cache:
            return self._results_cache[url]
        return ''
