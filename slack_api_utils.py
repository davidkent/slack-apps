from datetime import datetime, timedelta
from urllib import parse

import json
import re


JSON_KEY_RESPONSE_STATUS = "ok"
JSON_KEY_RESPONSE_ERROR = "error"
JSON_KEY_RESPONSE_WARNING = "warning"
JSON_KEY_RESPONSE_METADATA = "response_metadata"
JSON_KEY_NEXT_CURSOR = "next_cursor"

SLACK_MESSAGE_ARCHIVE_URL = "https://blackedgecapital.slack.com/archives/{}/p{}"

TAGGED_USER_MSG_END_RE = re.compile('<@(\w+)>$')


def datetime_from_slack_ts(ts):
    return datetime.fromtimestamp(float(ts))


def slack_link_for_message(channel, message):
    channel_id = channel.id
    msg_ts = str(message.post_time.timestamp()).replace('.', '')
    return SLACK_MESSAGE_ARCHIVE_URL.format(channel_id, msg_ts)


def handle_ok_response(url, response_text):
    print("GET[{}]: {}".format(url, response_text))
    response_json = json.loads(response_text)
    if JSON_KEY_RESPONSE_WARNING in response_json:
        response_warning = response_json[JSON_KEY_RESPONSE_WARNING]
        print("Warning[{}]: {}".format(url, response_warning))
    return response_json


def handle_error_response(url, response_text, status_code):
    response_json = json.loads(response_text)
    response_error = response_json.json()[JSON_KEY_RESPONSE_ERROR]
    print("GET[{}]: ERROR[{}] {}".format(url, status_code, response_error))
    raise ConnectionError(response_error)


FIRST_PAGE_NEXT_CURSOR = 'first'
LAST_PAGE_NEXT_CURSOR = ''
PAGINATION_LIMIT = 100
PAGINATION_PARAMS_LIMIT = 'limit'
PAGINATION_PARAMS_CURSOR = 'cursor'


def get_next_page_cursor(response_json):
    if JSON_KEY_RESPONSE_METADATA in response_json:
        response_metadata = response_json[JSON_KEY_RESPONSE_METADATA]
        if JSON_KEY_NEXT_CURSOR in response_metadata:
            next_cursor = response_metadata[JSON_KEY_NEXT_CURSOR]
            if len(next_cursor) > 0:
                return next_cursor

    return LAST_PAGE_NEXT_CURSOR


def encode_url_with_params_and_pagination(url, params, next_page_cursor=LAST_PAGE_NEXT_CURSOR):
    params[PAGINATION_PARAMS_LIMIT] = PAGINATION_LIMIT
    if next_page_cursor is not FIRST_PAGE_NEXT_CURSOR:
        params[PAGINATION_PARAMS_CURSOR] = next_page_cursor
    return encode_url_with_params(url, params)


def encode_url_with_params(url, params):
    return url + parse.urlencode(params)

# Definition of related message:
# - In a trade reports channel
# - occurs within 10 seconds of the previous message
# - previous message does not end with a username
# --<@U3TJ7HRK5>
def is_related_message(current_message, prior_message, channel_name):
    if not is_trade_report_channel(channel_name):
        return False
    if prior_message is None or current_message is None:
        return False
    if message_text_ends_with_tagged_user(prior_message.text):
        return False
    if prior_message.post_time + timedelta(seconds=10) < current_message.post_time:
        return False
    return True


def message_text_ends_with_tagged_user(msg_txt):
    return TAGGED_USER_MSG_END_RE.search(msg_txt) is not None


def extract_tagged_user_from_message_text_end(msg_txt):
    matched_text = TAGGED_USER_MSG_END_RE.search(msg_txt)
    return matched_text.group(1)


TRADE_REPORTS_CHANNEL_PREFIX = "trade_reports"


def is_trade_report_channel(channel_name):
    return channel_name.startswith(TRADE_REPORTS_CHANNEL_PREFIX)
