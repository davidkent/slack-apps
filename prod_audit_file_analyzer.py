from audit_data_extractor import AuditFileDataExtractor


class AuditFileAnalyzer:
    def __init__(self, audits):
        self._audits = audits
        self._extractor = None

    def load_from_file(self, filename):
        openfile = open(filename)
        file_lines = openfile.readlines()
        header = file_lines[0]
        self._extractor = AuditFileDataExtractor(header)
        for line in file_lines[1:]:
            self._add_line_to_audit(line)

    def _add_line_to_audit(self, line):
        audit_name = self._extractor.get_audit_name(line)
        if audit_name not in self._audits:
            return
        audit = self._audits[audit_name]
        self._extractor.add_data_to_audit(line, audit)
