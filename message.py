import datetime

import slack_api_utils
from slack_api_utils import datetime_from_slack_ts

JSON_KEY_TEXT = 'text'
JSON_KEY_TS = 'ts'
JSON_KEY_USER_ID = 'user'
JSON_KEY_PARENT_USER_ID = "parent_user_id"
JSON_KEY_REPLY_COUNT = 'reply_count'
JSON_KEY_REPLY_USER_COUNT = 'reply_users_count'
JSON_KEY_REPLY_USERS = 'reply_users'
JSON_KEY_LAST_REPLY_TS = 'latest_reply'
JSON_KEY_THREAD_TIMESTAMP = 'thread_ts'

NONE_THREAD_TS = 'NONE'

class Message:
    def __init__(self, message_json, channel, users):
        self.text = message_json[JSON_KEY_TEXT]
        self.channel = channel
        self._is_trade_report = slack_api_utils.is_trade_report_channel(channel.name)
        self.post_time = datetime_from_slack_ts(message_json[JSON_KEY_TS])
        self.created_by = users.get_user_for_id(message_json[JSON_KEY_USER_ID])
        self._set_user_given_message_text(users)
        self.related_messages = []
        self.reply_users = []
        if JSON_KEY_PARENT_USER_ID in message_json:
            self.thread_ts = message_json[JSON_KEY_THREAD_TIMESTAMP]
            self.reply_count = -1
            self.last_reply_time = datetime.MINYEAR
        elif JSON_KEY_THREAD_TIMESTAMP in message_json:
            self.thread_ts = message_json[JSON_KEY_THREAD_TIMESTAMP]
            self.reply_count = message_json[JSON_KEY_REPLY_COUNT]
            self.last_reply_time = datetime_from_slack_ts(message_json[JSON_KEY_LAST_REPLY_TS])
            reply_user_ids = message_json[JSON_KEY_REPLY_USERS]
            for reply_user_id in reply_user_ids:
                self.reply_users.append(users.get_user_for_id(reply_user_id))
        else:
            self.thread_ts = NONE_THREAD_TS
            self.reply_count = 0
            self.last_reply_time = datetime.MINYEAR

        self._thread = []

    def _set_user_given_message_text(self, users):
        if self._is_trade_report and slack_api_utils.message_text_ends_with_tagged_user(self.text):
            tagged_user = slack_api_utils.extract_tagged_user_from_message_text_end(self.text)
            self.user = users.get_user_for_id(tagged_user)
        else:
            self.user = self.created_by

    def _set_thread_details(self, related_message):
        if self.thread_ts is not NONE_THREAD_TS or related_message.thread_ts is NONE_THREAD_TS:
            return

        self.thread_ts = related_message.thread_ts
        self.reply_count = related_message.reply_count
        self.reply_users = related_message.reply_users
        self.last_reply_time = related_message.last_reply_time

    def add_related_message(self, related_message, users):
        additional_text = related_message.text
        self.text = self.text + "||||||||||\n" + additional_text
        self._set_user_given_message_text(users)
        self._set_thread_details(related_message)
        self.related_messages.append(related_message)

    def add_replies(self, replies_json_array, users):
        for reply_json in replies_json_array[1:]:
            reply = Message(reply_json, self.channel, users)
            self._thread.append(reply)

    def get_replies(self):
        return self._thread.copy()
