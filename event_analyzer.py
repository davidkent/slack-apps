# This scripts assumes that posts to the given channels are "production events" - events that occured in our production
# trading environment that required intervention and support by the tech team.
#
# It assumes each post to a channel in "incident_channel_names" is an incident, and that the resolution attemps were
# by the people that responded to that event in a thread.
#
# There is special handling for trade report channels as well.
#
# Finally, you can point it to a prior run's logfile and it will operate in "cache mode" - wherein all the HTTP requests
# it would normally send out to Slack will instead use the prior runs responses so it can operate completely locally.
# (Non-cached runs log the send/receive of all HTTP requests to a line in the log file).


import channels
import message_analyzer
import slack_api
import slack_http_cache
import sys
import users

OAUTH_TOKEN = "xoxb-15587958630-3081495650787-hDlyoIhm9VZeQCUbyyYeMPjr"


command_line_args = sys.argv
m_slack_api = slack_api.SlackApi(OAUTH_TOKEN)
if len(sys.argv) > 1:
    cache_file = sys.argv[1]
    slack_cache = slack_http_cache.SlackHttpCache()
    slack_cache.load_from_file(cache_file)
    m_slack_api.set_cache(slack_cache)

users_json = m_slack_api.get_all_users_json()
channels_json = m_slack_api.get_all_channels_json()

print("User count: {}".format(len(users_json)))
print("Channel count: {}".format(len(channels_json)))

my_users = users.Users(users_json, m_slack_api)
my_channels = channels.Channels(channels_json, my_users)

incident_channel_names = ['bug_reports', 'trade_reports']
# other channels - global_exceptions, trade_reports_high,

for name in incident_channel_names:
    channel = my_channels.get_channel_for_name(name)
    print("Downloading messages for {}[{}]".format(channel.name, channel.id))
    messages_json = m_slack_api.get_channel_messages_json(channel.id)
    channel.add_json_messages(messages_json, my_users)
    messages = channel.get_messages()
    print("--Message Count: {}".format(len(messages)))
    print("---------------------")
    message_analyzer.print_csv_header()
    for message in messages:
        if message.reply_count > 0:
            thread_json = m_slack_api.get_thread_messages_json(channel.id, message.thread_ts)
            message.add_replies(thread_json, my_users)
        message_analyzer.print_line_for_message(channel, message)