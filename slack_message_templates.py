BLOCK_KEY = "blocks"
BLOCK_TYPE_KEY = "type"
BLOCK_TYPE_SECTION = "section"
BLOCK_TYPE_DIVIDER = "divider"

MARKDOWN_KEY = "mrkdwn"
TEXT_KEY = "text"

PASSED_EMOJI = ":large_green_circle:"
FAILED_EMOJI = ":red_circle:"
ABSENT_EMOJI = ":ghost:"

DIVIDER_BLOCK = {BLOCK_TYPE_KEY: BLOCK_TYPE_DIVIDER}


def create_audit_slack_post(overall_audit_passed, individual_audit_results, audit_date):
    blocks = []
    absent_audits = []
    _add_overall_result(blocks, overall_audit_passed, audit_date)
    _add_divider(blocks)
    for result in individual_audit_results:
        if result.num_data_points() == 0:
            absent_audits.append(result)
        else:
            _add_individual_result(blocks, not result.threshold_breached, result)
    _add_absent_audits_block(blocks, absent_audits)
    return blocks


def _add_overall_result(blocks, overall_audit_passed, audit_date):
    result_markdown_template = "{:%Y-%m-%d} AUDIT RESULT {}"
    result_emoji = PASSED_EMOJI if overall_audit_passed else FAILED_EMOJI
    result_markdown = result_markdown_template.format(audit_date, result_emoji)
    blocks.append(_create_block_for_markdown_text(result_markdown))


def _add_individual_result(blocks, audit_passed, audit_detail):
    result_markdown_template = "{} `{}`"
    result_emoji = PASSED_EMOJI if audit_passed else FAILED_EMOJI
    audit_detail_text = "{}: {}/{} ({:.0f} {})".format(
        audit_detail.name, audit_detail.num_breaches(), audit_detail.num_data_points(),
        audit_detail.threshold, audit_detail.units)
    if not audit_passed:
        audit_detail_text = audit_detail_text + " Worst ({:.0f} {})".format(audit_detail.worst_breach(), audit_detail.units)
    result_markdown = result_markdown_template.format(result_emoji, audit_detail_text)
    blocks.append(_create_block_for_markdown_text(result_markdown))


def _add_absent_audits_block(blocks, absent_audits):
    if len(absent_audits) == 0:
        return
    _add_divider(blocks)
    absent_audits_markdown_template = "{} `{}`"
    absent_audit_names = [audit.name for audit in absent_audits]
    absent_detail_text = "Absent Audits: {} ({})".format(len(absent_audits), ", ".join(absent_audit_names))
    absent_markdown = absent_audits_markdown_template.format(ABSENT_EMOJI, absent_detail_text)
    blocks.append(_create_block_for_markdown_text(absent_markdown))

def _create_block_for_markdown_text(markdown_text):
    text_section = {BLOCK_TYPE_KEY: MARKDOWN_KEY,
                    TEXT_KEY: markdown_text}
    block = {BLOCK_TYPE_KEY: BLOCK_TYPE_SECTION,
             TEXT_KEY: text_section}
    return block


def _add_divider(blocks):
    blocks.append(DIVIDER_BLOCK)
