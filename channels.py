import message
import slack_api_utils

JSON_KEY_ID = 'id'
JSON_KEY_NAME = 'name'
JSON_KEY_MEMBER_COUNT = 'num_members'
JSON_KEY_CREATOR = 'creator'
JSON_KEY_PURPOSE = 'purpose'
JSON_KEY_TOPIC = 'topic'

JSON_KEY_HISTORY_MESSAGES = "messages"
JSON_KEY_THREAD_MESSAGES = "messages"


class Channel:
    def __init__(self, parsed_json, c_users):
        self.id = parsed_json[JSON_KEY_ID]
        self.name = parsed_json[JSON_KEY_NAME]
        self.member_count = parsed_json[JSON_KEY_MEMBER_COUNT]
        self.purpose = parsed_json[JSON_KEY_PURPOSE]
        self.creator = c_users.get_user_for_id(parsed_json[JSON_KEY_CREATOR])
        self._messages = []

    def __str__(self):
        return "Channel[{}]: {}({} Members), created by {}".format(self.id, self.name, self.member_count, self.creator.name)

    def add_json_messages(self, messages_json_arr, users):
        i = len(messages_json_arr)
        new_messages = []
        previous_message = None
        while i > 0:
            current_json_message = messages_json_arr[i - 1]
            current_message = message.Message(current_json_message, self, users)
            if slack_api_utils.is_related_message(current_message, previous_message, self.name):
                previous_message.add_related_message(current_message, users)
            else:
                new_messages.append(current_message)
                previous_message = current_message
            i = i - 1

        self._messages.extend(new_messages)

    def add_message(self, msg):
        self._messages.append(msg)

    def get_messages(self):
        return self._messages


class Channels:
    def __init__(self, channels_json_arr, users):
        self._channels = {}
        for channel in channels_json_arr:
            new_channel = Channel(channel, users)
            self._channels[new_channel.name] = new_channel

    def get_channel_for_name(self, channel_name):
        return self._channels[channel_name]
